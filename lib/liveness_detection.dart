import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:huawei_face_detection/huawei_face_detection.dart';
import 'package:camera/camera.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'dart:io' show Platform;

enum LivesnessAction {
  shakeTheHeadToTheLeft,
  shakeTheHeadToTheRight,
  nodTheHead,
  openTheMonth,
}

List<LivesnessAction> randomLivesnessActionSequence() {
  var sequences = [
    [LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.nodTheHead, LivesnessAction.openTheMonth, LivesnessAction.shakeTheHeadToTheRight],
    [LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.nodTheHead, LivesnessAction.openTheMonth, LivesnessAction.shakeTheHeadToTheLeft],
    [LivesnessAction.nodTheHead, LivesnessAction.openTheMonth, LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.shakeTheHeadToTheRight],
    [LivesnessAction.nodTheHead, LivesnessAction.openTheMonth, LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.shakeTheHeadToTheLeft],
    [LivesnessAction.openTheMonth, LivesnessAction.nodTheHead, LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.shakeTheHeadToTheRight],
    [LivesnessAction.openTheMonth, LivesnessAction.nodTheHead, LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.shakeTheHeadToTheLeft],
    [LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.openTheMonth, LivesnessAction.nodTheHead],
    [LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.openTheMonth, LivesnessAction.nodTheHead],
    [LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.nodTheHead, LivesnessAction.openTheMonth],
    [LivesnessAction.shakeTheHeadToTheRight, LivesnessAction.shakeTheHeadToTheLeft, LivesnessAction.nodTheHead, LivesnessAction.openTheMonth],
  ];

  var index = Random().nextInt(sequences.length);
  return sequences[index];
}

extension LivesnessActionExtension on LivesnessAction {
  int get id {
    switch (this) {
      case LivesnessAction.shakeTheHeadToTheLeft:
        // android flip the video when recorded using front camera so left become right
        if (Platform.isAndroid) return 2;
        return 1;
      case LivesnessAction.shakeTheHeadToTheRight:
        // android flip the video when recorded using front camera so right become left
        if (Platform.isAndroid) return 1;
        return 2;
      case LivesnessAction.nodTheHead:
        return 3;
      case LivesnessAction.openTheMonth:
        return 4;
    }
  }

  AssetImage get image {
    switch (this) {
      case LivesnessAction.shakeTheHeadToTheLeft:
        return const AssetImage('packages/huawei_face_detection/images/Card_Turn_left.png');
      case LivesnessAction.shakeTheHeadToTheRight:
        return const AssetImage('packages/huawei_face_detection/images/Card_Turn_right.png');
      case LivesnessAction.nodTheHead:
        return const AssetImage('packages/huawei_face_detection/images/Card_Nod_the_head_down.png');
      case LivesnessAction.openTheMonth:
        return const AssetImage('packages/huawei_face_detection/images/Card_Mouth_movement.png');
    }
  }
}

class LivenessDetectionApp extends StatefulWidget {
  const LivenessDetectionApp({
    required this.referenceCode,
    required this.onReceivedResult,
    super.key,
    required this.actions,
    required this.title,
    required this.hint,
    this.textStyle = const TextStyle(color: Colors.white, fontSize: 20.0),
    this.automaticallyImplyLeading = true,
  });

  final String referenceCode;
  final Function(FaceLiveDetectionResult?, String?) onReceivedResult;
  final List<LivesnessAction> actions;
  final String title;
  final String hint;
  final bool automaticallyImplyLeading;
  final TextStyle textStyle;

  @override
  State<LivenessDetectionApp> createState() => _LivenessDetectionAppState(onReceivedResult: onReceivedResult, actions: actions);
}

class _LivenessDetectionAppState extends State<LivenessDetectionApp> with WidgetsBindingObserver {
  late List<CameraDescription> _cameras;
  CameraController? controller;
  final Function(FaceLiveDetectionResult?, String?) onReceivedResult;
  final List<LivesnessAction> actions;
  XFile? videoFile;
  static const int delay = 3;

  _LivenessDetectionAppState({required this.onReceivedResult, required this.actions});

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    setupAvailableCamera();
  }

  Future<void> setupAvailableCamera() async {
    _cameras = await availableCameras();

    var camera = _cameras.firstWhere((element) => element.lensDirection == CameraLensDirection.front) ?? _cameras.first;

    controller = CameraController(camera, ResolutionPreset.medium);
    controller?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {
        startLivenessDetection();
      });
    }).catchError((Object e) {
      if (e is CameraException) {
        onReceivedResult(null, e.toString());
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();
    controller = null;
    Loader.hide();
    super.dispose();
  }

  // #docregion AppLifecycle
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
      controller = null;
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        setupAvailableCamera();
      }
    }
  }
  // #enddocregion AppLifecycle

  int _actionIndex = -1;
  late Timer _timer;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => widget.automaticallyImplyLeading,
        child: Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              title: Text(widget.title),
              automaticallyImplyLeading: widget.automaticallyImplyLeading,
            ),
            body: LayoutBuilder(
                builder: (context, constraints) => Column(children: <Widget>[
                      Container(
                        alignment: Alignment.topCenter,
                        height: constraints.maxHeight * 0.7,
                        child: _cameraPreviewWidget(),
                      ),
                      SizedBox(
                        height: constraints.maxHeight * 0.3,
                        child: _captureControlRowWidget(),
                      ),
                    ]))));
  }

  Widget _cameraPreviewWidget() {
    if (controller?.value.isInitialized == false) {
      return Container();
    }
    if (controller != null) {
      return CameraPreview(controller!,
          child: Stack(fit: StackFit.loose, alignment: Alignment.center, children: <Widget>[
            Container(
              padding: const EdgeInsets.all(60),
              alignment: Alignment.center,
              child: const Image(
                image: AssetImage('packages/huawei_face_detection/images/Face.png'),
                alignment: Alignment.center,
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              top: 20, // - ((previewWidth - 20) / cardRatio),
              left: 0,
              right: 0,
              child: Text(
                widget.hint,
                textAlign: TextAlign.center,
                maxLines: 2,
                style: widget.textStyle,
              ),
            )
          ]));
    }
    return Container();
  }

  Widget _captureControlRowWidget() {
    return Container(
      color: Colors.black,
      child: (_actionIndex > -1)
          ? Container(
              alignment: Alignment.center,
              child: Image(
                image: actions[_actionIndex].image,
                alignment: Alignment.center,
                height: double.infinity,
                width: double.infinity,
                fit: BoxFit.fitHeight,
              ),
            )
          : Container(),
    );
  }

  void startLivenessDetection() {
    startVideoRecording().then((_) {
      if (mounted) {
        setState(() {
          _actionIndex = 0;
          startTimer();
        });
      }
    });
  }

  void endLivenessDetection() {
    _timer.cancel();
    stopVideoRecording().then((XFile? file) {
      if (file != null) {
        controller?.pausePreview();
        videoFile = file;
        Loader.show(context, progressIndicator: const CircularProgressIndicator());
        _getFaceLivenessData(File(file.path)).then((val) {
          controller?.resumePreview();
          onReceivedResult(val, "");
          Loader.hide();
        }).catchError((error, stackTrace) {
          // error is SecondError
          debugPrint("endLivenessDetection outer: $error");
          controller?.resumePreview();
          onReceivedResult(null, error.toString());
          Loader.hide();
        });
      }
    });
  }

  void startTimer() {
    const duration = Duration(seconds: delay);
    _timer = Timer.periodic(
      duration,
      (Timer timer) {
        if (_actionIndex == actions.length - 1) {
          if (!mounted) {
            return;
          }
          setState(() {
            endLivenessDetection();
          });
        } else {
          if (!mounted) {
            return;
          }
          setState(() {
            _actionIndex++;
          });
        }
      },
    );
  }

  void showInSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 15),
    ));
  }

  void _showCameraException(CameraException e) {
    debugPrint(e.code);
    debugPrint(e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  Future<void> startVideoRecording() async {
    final CameraController? cameraController = controller;

    if (cameraController == null || !cameraController.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return;
    }

    if (cameraController.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return;
    }

    try {
      await cameraController.startVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return;
    }
  }

  Future<XFile?> stopVideoRecording() async {
    final CameraController? cameraController = controller;

    if (cameraController == null || !cameraController.value.isRecordingVideo) {
      return null;
    }

    try {
      return await cameraController.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  Future<FaceLiveDetectionResult> _getFaceLivenessData(File file) async {
    var ids = actions.map((e) => e.id.toString()).reduce((value, element) => '$value,$element');

    File imgfile = file;
    Uint8List imgbytes = await imgfile.readAsBytes();
    String videBase64 = base64.encode(imgbytes);

    return await getFaceLiveDetectionFromBase64(
      videoBase64: videBase64,
      actions: ids,
      referenceCode: widget.referenceCode,
    );
  }
}
