library huawei_face_detection;

export 'package:huawei_face_detection/huawei_services.dart';
export 'package:huawei_face_detection/thai_id_card_ocr.dart';
export 'package:huawei_face_detection/liveness_detection.dart';
