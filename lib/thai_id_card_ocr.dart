import 'dart:convert';
import 'dart:io';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter/material.dart';
import 'package:huawei_face_detection/huawei_face_detection.dart';
import 'package:camera/camera.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';

class ThaiIDCardOCRApp extends StatefulWidget {
  const ThaiIDCardOCRApp({
    required this.referenceCode,
    required this.onReceivedResult,
    required this.title,
    required this.hint,
    this.textStyle = const TextStyle(color: Colors.white, fontSize: 20.0),
    super.key,
  });

  final String referenceCode;
  final Function(OcrThailandIdCardResult?, String) onReceivedResult;
  final String title;
  final String hint;
  final TextStyle textStyle;

  @override
  State<ThaiIDCardOCRApp> createState() => _ThaiIDCardOCRCameraAppState();
}

class _ThaiIDCardOCRCameraAppState extends State<ThaiIDCardOCRApp> with WidgetsBindingObserver {
  late List<CameraDescription> _cameras;
  CameraController? controller;

  File? imageFile;
  _ThaiIDCardOCRCameraAppState();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    setupAvailableCamera();
  }

  Future<void> setupAvailableCamera() async {
    _cameras = await availableCameras();
    controller = CameraController(_cameras.first, ResolutionPreset.high);
    controller?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    }).catchError((Object e) {
      if (e is CameraException) {
        widget.onReceivedResult(null, e.toString());
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();
    controller = null;
    Loader.hide();
    super.dispose();
  }

  // #docregion AppLifecycle
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = controller;
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
      controller = null;
    } else if (state == AppLifecycleState.resumed) {
      setupAvailableCamera();
    }
  }
  // #enddocregion AppLifecycle

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    final CameraController? oldController = controller;
    if (oldController != null) {
      controller = null;
      await oldController.dispose();
    }
  }

  static const cardRatio = 1.65;

  @override
  Widget build(BuildContext context) {
    const containerSize = 100;
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: LayoutBuilder(
          builder: (context, constraints) => Stack(fit: StackFit.expand, children: <Widget>[
            Container(
              alignment: Alignment.topCenter,
              child: _cameraPreviewWidget(constraints.maxWidth),
            ),
            Positioned(
              top: constraints.maxHeight - containerSize,
              left: 0,
              right: 0,
              bottom: 0,
              child: _captureControlRowWidget(constraints.maxWidth),
            ),
          ]),
        ));
  }

  Widget _cameraPreviewWidget(double previewWidth) {
    if (controller?.value.isInitialized == false) {
      return Container();
    }
    if (controller != null) {
      return CameraPreview(
        controller!,
        child: Stack(
          fit: StackFit.loose,
          alignment: Alignment.topCenter,
          children: <Widget>[
            Positioned(
              top: 150,
              child: Container(
                width: previewWidth - 20,
                height: (previewWidth) / cardRatio,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('packages/huawei_face_detection/images/Scan-ID_Card.png'),
                    fit: BoxFit.fill,
                  ),
                  color: Colors.transparent,
                ),
                child: Container(),
              ),
            ),
            Positioned(
              top: 60, // - ((previewWidth - 20) / cardRatio),
              left: 0,
              right: 0,
              child: Text(
                widget.hint,
                textAlign: TextAlign.center,
                maxLines: 2,
                style: widget.textStyle,
              ),
            ),
            Positioned(
              top: 180 + ((previewWidth) / cardRatio), // - ((previewWidth - 20) / cardRatio),
              left: 0,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "ข้อแนะนำ",
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: widget.textStyle,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'packages/huawei_face_detection/images/li_icon.png',
                          width: 10,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          child: Text(
                            "หลีกเลี่ยงแสงสะท้อน และไม่มืดหรือสว่างเกินไป",
                            textAlign: TextAlign.start,
                            maxLines: 2,
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'packages/huawei_face_detection/images/li_icon.png',
                          width: 10,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          child: Text(
                            "รูปไม่เบลอ เห็นตัวอักษรชัดเจน และเห็นภาพเต็มใบ",
                            textAlign: TextAlign.start,
                            maxLines: 2,
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            (imageFile != null) ? Image.file(File(imageFile!.path)) : Container(),
          ],
        ),
      );
    }
    return Container();
  }
  // Positioned(
  // top: (constraints.maxHeight / 2),// - ((constraints.maxWidth - 20) / cardRatio), // above the frame
  // left: 0,
  // right: 0,
  // child: Text('กรุณาวางบัตรประชาชนให้ตรงกรอบ',
  // textAlign: TextAlign.center,
  // maxLines: 2,
  // style: TextStyle(fontSize: 20, color: Colors.red),
  // ),
  // ),

  Widget _captureControlRowWidget(double previewWidth) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 12, 0, 24),
      color: Colors.black,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ElevatedButton(
            onPressed: () {
              onTakePictureButtonPressed(previewWidth);
            },
            style: ElevatedButton.styleFrom(
              shape: const CircleBorder(),
              maximumSize: const Size(88, 88),
              backgroundColor: Colors.white, // <-- Button color
              foregroundColor: Colors.blue, // <-- Splash color
            ),
            child: Container(),
          ),
        ],
      ),
    );
  }

  void showInSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 15),
    ));
  }

  void _showCameraException(CameraException e) {
    debugPrint(e.code);
    debugPrint(e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  void onTakePictureButtonPressed(double previewWidth) {
    Loader.show(context, progressIndicator: const CircularProgressIndicator());
    takePicture().then((XFile? file) {
      if (mounted) {
        controller?.pausePreview();
        if (file != null) {
          _resizePhoto(file.path, previewWidth).then((File resizeFile) {
            // imageFile = resizeFile;
            _getCardInfo(resizeFile).then((val) {
              debugPrint(val.toString());
              controller?.resumePreview();
              widget.onReceivedResult(val, "");
              Loader.hide();
            }).catchError((error, stackTrace) {
              // error is SecondError
              debugPrint("outer: $error");
              controller?.resumePreview();
              widget.onReceivedResult(null, error.toString());
              Loader.hide();
            });
            setState(() {});
          });
        }
      }
    });
  }

  Future<XFile?> takePicture() async {
    final CameraController? cameraController = controller;
    if (cameraController == null || !cameraController.value.isInitialized) {
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      final XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      _showCameraException(e);
      controller?.resumePreview();
      widget.onReceivedResult(null, e.toString());
      return null;
    }
  }

  Future<File> _resizePhoto(String filePath, double previewWidth) async {
    ImageProperties properties = await FlutterNativeImage.getImageProperties(filePath);
    var imageWidth = properties.width ?? 0;
    var imageHeight = properties.height ?? 0;
    var boxPreviewWidth = previewWidth;
    var widthRatio = imageWidth / boxPreviewWidth;
    var cropImageWidth = (boxPreviewWidth * widthRatio) - (widthRatio * 20);
    var cropImageHeight = cropImageWidth / cardRatio;

    var xPos = widthRatio * 10;
    var yPos = (imageHeight / 2) - (cropImageHeight / 2);

    // if (cropImageWidth + xPos < imageWidth && cropImageHeight + yPos < imageHeight) {
    //   File croppedFile = await FlutterNativeImage.cropImage(filePath, xPos.toInt(), yPos.toInt(), cropImageWidth.toInt(), cropImageHeight.toInt());
    //   return croppedFile;
    // }

    return File(filePath);
  }

  Future<OcrThailandIdCardResult> _getCardInfo(File file) async {
    List<int> imageBytes = await file.readAsBytes();
    String base64Image = base64Encode(imageBytes);

    return await getThaiIDCardOCRFromBase64String(
      image: base64Image,
      referenceCode: widget.referenceCode,
    );
  }
}
