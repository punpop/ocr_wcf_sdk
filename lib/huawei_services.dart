library huawei_face_detection;

import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

var _env = HuaweiEnvironment();

String _endPoint = _env._endPoint;
String _appID = _env._appID;
String _appSecret = _env._appSecret;

class HuaweiEnvironment {
  factory HuaweiEnvironment() {
    return _singleton;
  }

  HuaweiEnvironment._internal();

  static final HuaweiEnvironment _singleton = HuaweiEnvironment._internal();

  String _endPoint = '';
  String _appID = '';
  String _appSecret = '';

  initConfig({
    required String endPoint,
    required String appID,
    required String appSecret,
  }) {
    _endPoint = endPoint;
    _appID = appID;
    _appSecret = appSecret;
  }
}

Future<OcrThailandIdCardResult> getThaiIDCardOCRFromBase64String({
  required String referenceCode,
  required String image,
}) async {
  Map data = {'id_card_image': image, "reference_code": referenceCode};

  return _sendThaiIDCardOCRData(data);
}

Future<OcrThailandIdCardResult> _sendThaiIDCardOCRData(
  Map data,
) async {
  final response =
      await http.post(Uri.parse('$_endPoint/api/ocr_id_card_for_sdk'),
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'app_id': _appID,
            'app_secret': _appSecret
          },
          body: json.encode(data));
  if (response.statusCode == 200) {
    return OcrThailandIdCardResult.fromJson(
        json.decode(utf8.decode(response.bodyBytes)));
  } else {
    throw Exception(Error.fromJson(json.decode(utf8.decode(response.bodyBytes)))
        .toString());
  }
}

Future<FaceLiveDetectionResult> getFaceLiveDetectionFromBase64({
  required String referenceCode,
  required String videoBase64,
  required String actions,
}) async {
  Map data = {
    "reference_code": referenceCode,
    'video_base64': videoBase64,
    'actions': actions
  };
  return _sendFaceLiveDetection(data);
}

Future<FaceLiveDetectionResult> _sendFaceLiveDetection(
  Map data,
) async {
  final response = await http.post(
      Uri.parse('$_endPoint/api/liveness_detection_and_face_compare_for_sdk'),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'app_id': _appID,
        'app_secret': _appSecret,
      },
      body: json.encode(data));

  if (response.statusCode == 200) {
    return FaceLiveDetectionResult.fromJson(
        json.decode(utf8.decode(response.bodyBytes)));
  } else {
    throw Exception(Error.fromJson(json.decode(utf8.decode(response.bodyBytes)))
        .toString());
  }
}

class Error {
  final int code;
  final String message;

  @override
  String toString() => 'code: $code, message: $message';

  const Error({required this.code, required this.message});

  factory Error.fromJson(Map<String, dynamic> body) {
    return Error(
      code: body['code'],
      message: body['message'],
    );
  }
}

class OcrThailandIdCardResult {
  int? code;
  String? message;
  OcrThailandIdCardData? data;

  OcrThailandIdCardResult({this.code, this.message, this.data});

  OcrThailandIdCardResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null
        ? new OcrThailandIdCardData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data?.toJson();
    }
    return data;
  }
}

class OcrThailandIdCardData {
  String? idCardNo;

  OcrThailandIdCardData({this.idCardNo});

  OcrThailandIdCardData.fromJson(Map<String, dynamic> json) {
    idCardNo = json['id_card_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_card_no'] = this.idCardNo;
    return data;
  }
}

class FaceLiveDetectionResult {
  FaceLiveDetectionResult({
    required this.code,
    required this.message,
    required this.data,
  });
  late final int code;
  late final String message;
  late final FaceLiveDetectionData data;

  FaceLiveDetectionResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = FaceLiveDetectionData.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['code'] = code;
    _data['message'] = message;
    _data['data'] = data.toJson();
    return _data;
  }
}

class FaceLiveDetectionData {
  FaceLiveDetectionData({
    required this.alive,
    required this.faceCompareResult,
    required this.faceCompareSimilarity,
  });
  late final bool alive;
  late final bool faceCompareResult;
  late final double faceCompareSimilarity;

  FaceLiveDetectionData.fromJson(Map<String, dynamic> json) {
    alive = json['alive'];
    faceCompareResult = json['face_compare_result'];
    faceCompareSimilarity = json['face_compare_similarity'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['alive'] = alive;
    _data['face_compare_result'] = faceCompareResult;
    _data['face_compare_similarity'] = faceCompareSimilarity;
    return _data;
  }
}
