import 'package:flutter/material.dart';
import 'package:huawei_face_detection/huawei_face_detection.dart';
import 'constants.dart';
import 'package:uuid/uuid.dart';

var uuid = const Uuid();

const _endPoint = 'https://sso-dev.blockfint.com';
const _appID = 'WCF';
const _appSecret = '4yqD1KqyChaGu13UW6wN9uc6aLRsk5vZ5FXLJcmVaWPR6KtM3fcNGiVnKgDF74VLDadDHEizpgMWiTkkT2PsqEZm';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // setup environment
  var env = HuaweiEnvironment();
  env.initConfig(
    endPoint: _endPoint,
    appID: _appID,
    appSecret: _appSecret,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Demo', theme: ThemeData(primarySwatch: Colors.blue), home: const MyHomePage(title: 'SDK Demo'));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String displayText = '';
  String refCode = '';
  String? idCardNo;
  bool enableOCRButton = false;
  bool enableLivenessButton = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<String>(
              builder: (context, snapshot) {
                return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Text('Ref code: $refCode'),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                  ),
                  SizedBox(
                    height: 40,
                    width: 260,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          refCode = uuid.v4();
                          idCardNo = null;
                          displayText = '';
                          enableOCRButton = true;
                        });
                      },
                      child: const Text('Generate New Reference Code'),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                  ),
                  SizedBox(
                    height: 40,
                    width: 260,
                    child: ElevatedButton(
                      onPressed: refCode.isNotEmpty && enableOCRButton
                          ? () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ThaiIDCardOCRApp(
                                          referenceCode: refCode,
                                          title: 'สแกนหน้าบัตรประชาชน',
                                          hint: 'กรุณาวางบัตรประชาชนให้ตรงกรอบ',
                                          onReceivedResult: (
                                            ocrRes,
                                            error,
                                          ) {
                                            String text = 'OCR error: ${error.toString()}\n\n'
                                                'ID card no: ${ocrRes?.data?.idCardNo}\n\n';

                                            Navigator.of(context).popUntil(
                                              (route) => route.isFirst,
                                            );

                                            setState(() {
                                              idCardNo = ocrRes?.data?.idCardNo;
                                              displayText = text;
                                              enableOCRButton = false;
                                              enableLivenessButton = true;
                                            });
                                          },
                                        )),
                              );
                            }
                          : null,
                      child: const Text('OCR Thailand ID Card'),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                  ),
                  SizedBox(
                    height: 40,
                    width: 260,
                    child: ElevatedButton(
                      onPressed: idCardNo != null && enableLivenessButton
                          ? () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LivenessDetectionApp(
                                          referenceCode: refCode,
                                          title: 'ตรวจใบหน้า',
                                          hint: 'กรุณาวางใบหน้าให้อยู่ในกรอบ',
                                          actions: randomLivesnessActionSequence(),
                                          onReceivedResult: (
                                            livenessRes,
                                            error,
                                          ) {
                                            if (error != null) {
                                              displayText += 'Liveness error: ${error.toString()}\n\n';
                                              setState(() {
                                                displayText = displayText;
                                              });
                                            } else {
                                              displayText += 'Alive: ${livenessRes?.data.alive}\n\n';
                                              displayText += 'Similarity: ${livenessRes?.data.faceCompareSimilarity}\n\n';

                                              setState(() {
                                                displayText = displayText;
                                                enableLivenessButton = false;
                                              });
                                            }
                                            Navigator.of(context).popUntil(
                                              (route) => route.isFirst,
                                            );
                                          },
                                        )),
                              );
                            }
                          : null,
                      child: const Text('Liveness Detection & Face Compare'),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Text(displayText),
                  ),
                ]);
              },
            ),
          ],
        ),
      ),
    );
  }
}
