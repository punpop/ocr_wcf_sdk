
## Getting started

start using the package.
```
dependencies:
    huawei_face_detection:
        path: ../huawei_face_detection
```

```dart
import 'package:huawei_face_detection/huawei_face_detection.dart';
```

Config Environment
```dart
HuaweiEnvironment().initConfig(
projectId: '_projectId',
user: '_user',
privateKey: '_privateKey or password',
domain: '_domain',
projectName: '_projectName',
frsEndPoint: '_frsEndPoint',
iamEndPoint: '_iamEndPoint',
ocrEndPoint: '_ocrEndPoint',
);
```

## Setting up Camera
For the Camera feature this package required [Camera Plugin](https://pub.dev/packages/camera), Please refer these setup for [iOS](https://pub.dev/packages/camera#ios) and [Android](https://pub.dev/packages/camera#android).

### App usage
IdentityVerificationApp
```
IdentityVerificationApp(
    title: 'Identity Verification',
    // access token from getAuthToken() goes here;
    token: '...',
    
    // title for each step
    idCardOCRTitle: 'สแกนหน้าบัตรประชาชน',
    faceComparisonTitle: 'สแกนใบหน้า',
    livenessDetectionTitle: 'ตรวจใบหน้า',
    // error messages for each step
    idCardOCRErrorMessage: 'เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง',
    faceComparisonErrorMessage: 'เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง',
    livenessDetectionErrorMessage: 'เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง',
    
    // hint message above the frame
    idCardOCRHint: 'กรุณาวางบัตรประชาชนให้ตรงกรอบ',
    faceComparisonHint: 'กรุณาวางใบหน้าให้อยู่ในกรอบ',
    livenessDetectionHint: 'กรุณาวางใบหน้าให้อยู่ในกรอบ',
    
    // font style for the hint mesasge eg. color, size, font family..
    textStyle: const TextStyle(color: Colors.white, fontSize: 20),
                                    
    // retry count for each step
    idCardOCRRetryCount: 3,
    faceComparisonRetryCount: 3,
    livenessDetectionRetryCount: 3,
    
    // Minimum similarity threshold for face comparison
    faceComparisonSimilarityThreshold: 0.2,
    
    // Result call back
    // thailandIdCardResult: result of ID Card OCR step
    // faceComparisonResult: result of FaceComparison step
    // imageFile: the selfie image
    // faceLiveDetectionResult: result of live detection
    // error: error if any
    onReceivedResult: (thailandIdCardResult, faceComparisonResult, imageFile, faceLiveDetectionResult, error) {

        String text = 'ThailandIdCardResult: ${thailandIdCardResult?.firstNameEn} ${thailandIdCardResult?.lastNameEn}\n\n';
        text += 'faceComparisonResult: similarity ${faceComparisonResult?.similarity}\n\n';
        text += 'imageFile: ${imageFile?.path}\n\n';
        text += 'faceLiveDetectionResult: alive ${faceLiveDetectionResult?.alive}\n\n';
        text += 'error: ${error.toString()}\n\n';
        showInSnackBar(text);

        Navigator.of(context).popUntil((route) => route.isFirst);
    }
)
```

ThaiIDCardOCRApp
```
ThaiIDCardOCRApp(
    
    // Result call back
    onReceivedResult: (result , error) {
      if(result != null) {
        
        // received ID card result
        showInSnackBar('Hello: ${result.firstNameEn} ${result.lastNameEn}');
      } else if(error != null) {
        
        // received error result
        showInSnackBar('error $error');
      }
    },
    
    // access token from getAuthToken() goes here;
    token: '....',
    
    // title
    title: 'สแกนหน้าบัตรประชาชน',
)
```

FaceComparisonApp
```
FaceComparisonApp(
    
    // Result call back
    onReceivedResult: (result, image,  error) {
      if(result != null) {
        
        // received face comparison result
        showInSnackBar('similarity: ${result.similarity}');
      } else if(error != null) {
        
        // received error result
        showInSnackBar('error $error');
      }
    },
    
    // access token from getAuthToken() goes here;
    token: '....',
    
    // title
    title: 'สแกนใบหน้า',
    
    // Base64 Sring of the image or an image file
    // send either one of these
    comparisonBase64: elonMuskBase64,
    comparisonFile: null,
)
```

LivenessDetectionApp
```
LivenessDetectionApp(
  
  // Result call back
  onReceivedResult: (result , error) {
    if(result != null) {
      
      // received liveness result
      showInSnackBar('alive: ${result.alive}'); 
    } else if(error != null) {
      
      // received error result
      showInSnackBar('error $error');
    }
  },
  
  // access token from getAuthToken() goes here;
  token: '......',
 
  // Actions for verify liveness
  // random actions can be set using randomLivesnessActionSequence() 
  actions: const [
    LivesnessAction.shakeTheHeadToTheLeft,
    LivesnessAction.shakeTheHeadToTheRight,
    LivesnessAction.nodTheHead,
    LivesnessAction.openTheMonth
  ],
  
  // title
  title: 'Liveness Detection',
)
```

please check `example/lib/main.dart` for the full code example.

## API Usage

Get Auth token from the service - this token is required for making other api calls
```dart
getAuthToken();
```

### Thai ID card OCR
Get Thai ID Car OCR from Image Url
```dart
getThaiIDCardOCRFromUrl(imageUrl: 'https://example.com/image.jpg', token: 'token');
```

Get Thai ID Car OCR from Base64String
```dart
getThaiIDCardOCRFromBase64String(image: '..U2I7gBktRWdWDRGjnAd17rdKAFEPv3Q9EwR3ssJ48h3W4gUA==', token: 'token');
```

Optional parameters
```dart
/// Front or back of an ID card. Value options are 'front' and 'back'.
String side = 'front',

/// Whether to return the Base64 code of the portrait on the ID card image. Value options are as follows: true, false
    bool returnPortraitImage = false,

/// Whether to return the ID card type. Value options are as follows: true, false
bool returnIdCardType = false,
```

### Face Detection
Get Face Detection from image url
```dart
getFaceDetectionDataFromImageUrl(imageUrl: 'https://example.com/image.jpg', token: 'token');
```

Get Face Detection from base64 string
```dart
getFaceDetectionDataFromBase64String(image: '..U2I7gBktRWdWDRGjnAd17rdKAFEPv3Q9EwR3ssJ48h3W4gUA==', token: 'token');
```

Get Face Detection from File
```dart
getFaceDetectionDataFromFile(imageFile: File('../image.png'), token: 'token');
```

### Face Comparison
Get Face Comparison from two Image Urls
```dart
getFaceComparisonDataFromUrl(url1: 'https://example.com/image1.jpg', url2: 'https://example.com/image1.jpg', token: 'token');
```

Get Face Comparison from two base64 strings
```dart
getFaceComparisonDataFromBase64String(image1: '..UA==', image2: '..U2I7g==', token: 'token');
```

Get Face Comparison from two files
```dart
getFaceComparisonDataFromFile(imageFile1: File('../image1.png'), imageFile2: File('../image2.png') token: 'token');
```


### Face Live Detection
Get Face Live Detection from video Url
```dart
getFaceLiveDetectionFromUrl(url: 'https://example.com/video.mp4', actions: '1,3,2', token: 'token');
```

Get Face Live Detection from base64 string
```dart
getFaceLiveDetectionFromBase64String(video: '..U2I7gBktRWdWDRGjnAd17rdKAFEPv3Q9EwR3ssJ48h3W4gUA', actions: '1,3,2', token: 'token');
```

Get Face Detection from file
```dart
getFaceLiveDetectionFromFile(videoFile: File('../videoFile.mp4'), actions: '1,3,2', token: 'token');
```

Optional parameters
```dart
/// Action code sequence list. Actions are separated by commas (,). Currently, the following actions are supported:
/// 1: Shake the head to the left.
/// 2: Shake the head to the right.
/// 3: Nod the head.
/// 4: Mouth movement
String actions = '1,3,2',

/// String of the action time array. The length of the array is the same as the number of actions.
/// Each item contains the start time and end time of the action in the corresponding sequence. 
/// The unit is the milliseconds from the video start time.
    String actionTime = '1000-3000,4000-7000,9000-12000',
```

https://support.huaweicloud.com/intl/en-us/api-face/face_02_0052.html